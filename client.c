#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <alsa/asoundlib.h>

#include "client.h"

int check_server;
time_t rawtime;
struct tm * timeinfo;
int fd_conn_udp_h;
int fd_conn_udp;
struct sockaddr_in server_struct_udp_h;
struct sockaddr_in server_struct_udp;
msg* struct_msg;
msg* struct_msg_recv;
pthread_t thread;
char server_ip[16];

//Error handler
void error(char* msg){
	printf("Error in %s\n",msg);
	exit(EXIT_FAILURE);
}

//Thread for sending messages
void* handler(void* args){
		
	while(1){
		recevitor_messagesUDP(fd_conn_udp_h,&server_struct_udp_h);
	}
	pthread_exit(NULL);
}

//Handler of sigint
void sig_handler(int sig){

	pthread_cancel(thread);
	pthread_join(thread,NULL);

	//Tell that the client is leaving the chat
	if(check_server!=0){

		int fromlen=sizeof(server_struct_udp);

		strcpy(struct_msg->txt,QUIT);

		//Send msg to the server
		int ret = sendto(fd_conn_udp,struct_msg,2048,0,(struct sockaddr *)&server_struct_udp,fromlen);
		if (ret == -1) error(strerror(errno));
	}


	//Free all resources and close fd
	free(struct_msg);
	free(struct_msg_recv);

	int ret=close(fd_conn_udp);
	if(ret==-1) error("Closing a socket");
	ret=close(fd_conn_udp_h);
	if(ret==-1) error("Closing a socket");

	exit(EXIT_SUCCESS);
}

void sending_messagesUDP(int fd_conn,struct sockaddr_in* server_struct){

	//Command to close the program
	char* quit_command=QUIT;
	int quit_command_len=strlen(quit_command);

	int ret;
	int finish=1;

	char* aux;
	char txt[1024];
	memset(txt,0,sizeof(txt));

	//Read from shell
	if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

	int msg_len=strlen(txt);
	
	txt[msg_len - 1] = '\0';

	//If 'Quit' is written so send it to server and close the program
	finish=memcmp(txt,quit_command,quit_command_len);

	aux=strchr(txt,'-');
	if(aux!=NULL && strlen(aux)>1  && check_server!=0){

		strcpy(struct_msg->txt,txt);

		int fromlen=sizeof(*server_struct);

		//Sendo msg to the server
		ret = sendto(fd_conn,struct_msg,2048,0,(struct sockaddr *)server_struct,fromlen);
		if (ret == -1) error(strerror(errno));
	}
	//Chekc if 'Quit' is written
	else if(finish==0){

		//Check if the Server is still open
		if(check_server!=0){

			strcpy(struct_msg->txt,txt);

			int fromlen=sizeof(*server_struct);

			//Sendo msg to the server
			ret = sendto(fd_conn,struct_msg,2048,0,(struct sockaddr *)server_struct,fromlen);
			if (ret == -1) error(strerror(errno));

		}
		
		//Free all resources and close fd
		free(struct_msg);
		free(struct_msg_recv);

		pthread_cancel(thread);
		pthread_join(thread,NULL);

		ret=close(fd_conn_udp);
		if(ret==-1) error("Closing a socket");
		ret=close(fd_conn_udp_h);
		if(ret==-1) error("Closing a socket");

		exit(EXIT_SUCCESS);

	}else if(check_server!=0){
		printf("Message form is 'Addressee-Messagge' \n");
	}else{
		printf("Server is closed,please quit the program\n");
	}

}

void sending_messagesTCP(int fd_conn,char* msg){

	int msg_len=strlen(msg);
	int bytes_send=0;

	int ret;

	//deal with partially sent messages
	while(bytes_send<msg_len){
		ret = send(fd_conn, msg + bytes_send, msg_len - bytes_send,0);
		if (ret == -1 && errno == EINTR) continue;
    	if (ret == -1) error("Writing the socket");
    	bytes_send += ret;
	}

}

void recevitor_messagesUDP(int fd_conn,struct sockaddr_in* server_struct){
	
	int ret;
	char* user;

	socklen_t len_struct=sizeof(struct sockaddr_in);

	ret=recvfrom(fd_conn,struct_msg_recv,sizeof(msg),0,NULL,NULL);
	if (ret == -1) error("Reading from the socket");
	
	//Check if message is from the server that tells the list of users
	if(strcmp(struct_msg_recv->name,"list")==0){
		printf("USERS ONLINE:[");
		user=strtok(struct_msg_recv->txt,"-");
		while(user){
			printf("|%s|",user);
			user=strtok(NULL,"-");
		}
		printf("]\n");
	}else if(strcmp(struct_msg_recv->name,SERVER_CLOSE)==0){

		printf("%s,please quit the program\n",struct_msg_recv->name);
		check_server=0;	

		pthread_exit(NULL);
	}
	//if the server is sending an error message 
	else if(strcmp(struct_msg_recv->name,USER_ERROR)==0){
		printf("%s\n",struct_msg_recv->name);
	}else if(strcmp(struct_msg_recv->name,NOT_ONLINE_ERROR)==0){
		printf("%s\n",struct_msg_recv->name);

	//Or if the server is sending a message from another user
	}else{

		//Print the time
		time( &rawtime );
		timeinfo = localtime( &rawtime );

		printf("\n[%02d:%02d:%02d-%s]:%s\n",timeinfo->tm_hour, timeinfo->tm_min,timeinfo->tm_sec,struct_msg_recv->name,struct_msg_recv->txt);
	}


}

void recevitor_messagesTCP(int fd_conn,char* msg){
	
	int ret;

	int bytes_recvs=0;

	//Deal with partially recv messages
	do{
		ret=recv(fd_conn,msg+bytes_recvs,1,0);
		if (ret == -1 && errno == EINTR) continue;
        	if (ret == -1) error("Reading from the socket");
        	if (ret == 0) break;
		bytes_recvs++;
	}while(msg[bytes_recvs-1]!='\n');

	msg[bytes_recvs-1]='\0';

}

int setUpConnectionTCP(struct sockaddr_in *server_struct,int sockaddr_len){

    int ret;

    int fd_conn=socket(AF_INET, SOCK_STREAM, 0);
    if(fd_conn==-1) error("Creating the socket");

    ret = connect(fd_conn, (struct sockaddr*) server_struct, sockaddr_len);
    if(ret==-1) error("Connecting to the server"); 

    return fd_conn;
}

void Login(int fd_conn,int fd_conn_udp,struct sockaddr_in *server_struct){

	int ret;

	char password[1024];
	char username[1024];
	char result[1024];
	int login_result;

    memset(password,0,sizeof(password));
  	memset(username,0,sizeof(username));
  	memset(result,0,sizeof(result));
  	

	printf("Insert username: ");

	if(fgets(username, sizeof(username), stdin)!=(char*)username) error("Reading from shell");

	int msg_username=strlen(username);
	
	username[msg_username - 1] = '\n';

	sending_messagesTCP(fd_conn,username);

	printf("Insert password: ");

	if(fgets(password, sizeof(password), stdin)!=(char*)password) error("Reading from shell");

	int msg_password=strlen(password);

	password[msg_password - 1] = '\n';

	sending_messagesTCP(fd_conn,password);

	int fromlen=sizeof(*server_struct);

	recevitor_messagesTCP(fd_conn,result);

	printf("%s\n",result);

	//If the Login is OK send a fake msg to allow the server to save struct of the client
	if((strcmp(result,ERROR)!=0 && strcmp(result,ERROR_PASS)!=0)){
		username[msg_username - 1] = '\0';
		strcpy(struct_msg->name,username);
		ret = sendto(fd_conn_udp," ",1024,0,(struct sockaddr *)server_struct,fromlen);
		if (ret == -1) error(strerror(errno));
		return;
	}
	else{
		free(struct_msg);
		free(struct_msg_recv);
		exit(EXIT_FAILURE);
	}


}

int setUpConnectionUDP(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	server_struct->sin_family =AF_INET;                    
    server_struct->sin_addr.s_addr = inet_addr(server_ip);    
    server_struct->sin_port = htons(NUM_DOOR_UDP);

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Creating a new socket");

	return fd;

}

int setUpConnectionUDP_H(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	server_struct->sin_family =AF_INET;                    
    server_struct->sin_addr.s_addr = inet_addr(server_ip);    
    server_struct->sin_port = htons(NUM_DOOR_UDP_H);

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Creating a new socket");

	return fd;

}

int main(int argc, char** argv){

	int ret;
	check_server=0;

	struct_msg=malloc(sizeof(msg));
	if(struct_msg==NULL) error("Allocate memory");
	memset(struct_msg,0,sizeof(msg));
	struct_msg_recv=malloc(sizeof(msg));
	if(struct_msg_recv==NULL) error("Allocate memory");
	memset(struct_msg_recv,0,sizeof(msg));

	strcpy(server_ip,ADDR_SERVER);

	if(argc==2){
		strcpy(server_ip,argv[1]);
	}

	struct sigaction act={0};
	act.sa_handler=sig_handler;

	//Install handler of signals
	ret=sigaction(SIGINT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGQUIT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTERM,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTSTP,&act,NULL);
	if(ret==-1) error("sigaction");

    struct sockaddr_in server_struct_tcp = {0};
    memset(&server_struct_udp,0,sizeof(struct sockaddr_in));

    int sockaddr_len = sizeof(struct sockaddr_in);

    memset(&server_struct_udp,0,sizeof(struct sockaddr_in));

    //Insert the address of the server
    server_struct_tcp.sin_addr.s_addr = inet_addr(server_ip);

    //Only IPv4
    server_struct_tcp.sin_family = AF_INET;

    //To convert host byte order
    server_struct_tcp.sin_port = htons(NUM_DOOR_TCP); 

    int fd_conn_tcp=setUpConnectionTCP(&server_struct_tcp,sockaddr_len);

    fd_conn_udp=setUpConnectionUDP(&server_struct_udp,sockaddr_len);

    fd_conn_udp_h=setUpConnectionUDP_H(&server_struct_udp_h,sockaddr_len);

    Login(fd_conn_tcp,fd_conn_udp_h,&server_struct_udp_h);

    check_server=1;

    ret=close(fd_conn_tcp);
    if(ret==-1) error("Closing TCP connection");

    printf("\n");
	printf("Welcome to this chat,the message form must be 'Addressee-Messagge',using 'All' as Addressee you can send a broadcast,if you want to leave the chat please send 'Quit' \n");
	printf("\n");

    //Create a thread to send messages
    ret=pthread_create(&thread,NULL,handler,NULL);
    if(ret==-1) error("Creating the thread");

    //A while fot receve messages
	while(1){
		sending_messagesUDP(fd_conn_udp,&server_struct_udp);
	}

    

	return 0;


}
