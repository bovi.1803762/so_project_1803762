#define ADDR_SERVER "127.0.0.1"
#define QUIT "Quit"
#define ERROR_PASS "Password wrong"
#define ERROR "This username is already used"
#define USER_ERROR "You cannot send a message to yourself"
#define NOT_ONLINE_ERROR "This user is not online now"
#define SERVER_CLOSE "Server is close"
#define NUM_DOOR_TCP 2902
#define NUM_DOOR_UDP 2903
#define NUM_DOOR_UDP_H 2904
#define LOGIN_OK 1

//Struct for mesaages
typedef struct msg{
	char name[1024]; //Name client
	char txt[1024];  //Txt 
}msg;

int setUpConnectionTCP(struct sockaddr_in *server_struct,int sockaddr_len);
int setUpConnectionUDP(struct sockaddr_in *server_struct,int sockaddr_len);
void error(char* msg);
void Login(int fd_conn,int fd_conn_udp,struct sockaddr_in *server_struct);
void sending_messagesUDP(int fd_conn,struct sockaddr_in* server_struct);
void recevitor_messagesUDP(int fd_conn,struct sockaddr_in* server_struct);
void sending_messagesTCP(int fd_conn,char* msg);
void recevitor_messagesTCP(int fd_conn,char* msg);
int setUpConnectionUDP_H(struct sockaddr_in *server_struct,int sockaddr_len);