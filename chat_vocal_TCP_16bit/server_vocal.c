#include <sys/socket.h>
#include <sys/types.h> 
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <stdlib.h>
#include <signal.h>

#include "server.h"
#include "linked_list.h"

typedef struct msg_vocal{
	char addressee[1024];
	char sender[1024];
}msg_vocal;

UserHead* user_list;
msg* struct_msg;
msg* struct_msg_send;
thread_args* args;
char* list_users;

int fd_socket_udp_h;
int fd_socket_udp;

time_t rawtime;
struct tm * timeinfo;
pthread_t thread;


//Error handler
int error(char* msg){
	printf("Error in %s \n",msg);
	exit(EXIT_FAILURE);
}

int HandleVocalMessages(int fd_tcp,char* username){

	int ret;

	int size_audio;

	ret=recv(fd_tcp,&size_audio,sizeof(size_audio),0);
	if(ret==-1) error(strerror(errno));
	if(ret==0) return 0;

	printf("Audio size: %d \n",size_audio);

	//char audio_name[1024]="audio_";
	//strcat(audio_name,username);
	//strcat(audio_name,".raw");

	//int fd_file=open(audio_name,O_CREAT | O_TRUNC | O_RDWR,0666);
	//if(fd_file==-1){ printf("Open file"); exit(0);}

	char addresse[1024];
	memset(addresse,0,1024);

	msg_vocal *vocal_struct=malloc(sizeof(msg_vocal));
	if(vocal_struct==NULL) error("Allocating memory");
	memset(vocal_struct,0,2048);

	int recv_bytes=0;
	int ret_wr;

	ret=recv(fd_tcp,vocal_struct,2048,0);
	if(ret==-1) error(strerror(errno));

	char buffer_audio[size_audio];
	memset(buffer_audio,0,size_audio);

	while(recv_bytes<size_audio){
		ret=recv(fd_tcp,buffer_audio+recv_bytes,size_audio-recv_bytes,0);
		if(ret==-1) error(strerror(errno));
		recv_bytes+=ret;
	}

	//printf("len file %ld \n",sizeof(buffer_audio));

	if(strcmp(vocal_struct->addressee,"All")==0){

		printf("SENDING VOCAL MESSAGE OF [%d] BYTE FROM [%s] TO [%s] \n",size_audio,vocal_struct->sender,vocal_struct->addressee);

		UserListItem* aux=user_list->head;
		while(aux!=NULL){

			if(strcmp(aux->username,vocal_struct->sender)!=0){
				int fd_tcp_addr=Find_client_fd(user_list,aux->username);

				int written_bytes=0;

				if(fd_tcp_addr!=0){

					ret=send(fd_tcp_addr,&size_audio,sizeof(size_audio),0);
					if(ret==-1) error(strerror(errno));

					//printf("qui\n");

					ret=send(fd_tcp_addr,vocal_struct,2048,0);
					if(ret==-1) error(strerror(errno));

					while(written_bytes<size_audio){
						ret=send(fd_tcp_addr,buffer_audio+written_bytes,size_audio-written_bytes,0);
						if(ret==-1) error(strerror(errno));
						written_bytes+=ret;
					}
				}
			}

			aux=aux->next;
		}

	}
	else{

		printf("SENDING VOCAL MESSAGE OF [%d] BYTE FROM [%s] TO [%s] \n",size_audio,vocal_struct->sender,vocal_struct->addressee);

		int fd_tcp_addr=Find_client_fd(user_list,vocal_struct->addressee);
		//printf("fd addressee: %d\n",fd_tcp_addr);

		int written_bytes=0;

		if(fd_tcp_addr!=0){

			//printf("into\n");

			ret=send(fd_tcp_addr,&size_audio,sizeof(size_audio),0);
			if(ret==-1) error(strerror(errno));

			//printf("qui\n");

			ret=send(fd_tcp_addr,vocal_struct,2048,0);
			if(ret==-1) error(strerror(errno));

			while(written_bytes<size_audio){
				ret=send(fd_tcp_addr,buffer_audio+written_bytes,size_audio-written_bytes,0);
				if(ret==-1) error(strerror(errno));
				written_bytes+=ret;
			}
		}

	}

	free(vocal_struct);

	return 1;

}

void sig_handler(int sig){
	pthread_cancel(thread);
	pthread_join(thread,NULL);

	//First of all send a message to all users online that the server is closing
	UserListItem* aux=user_list->head;
	while(aux){
		
		int fromlen=sizeof(*(aux->client_addr));

		strcpy(struct_msg_send->name,SERVER_CLOSE);
		strcpy(struct_msg_send->txt,"");

		int ret = sendto(fd_socket_udp_h,struct_msg_send,sizeof(msg),0,(struct sockaddr *)aux->client_addr,fromlen);
    	if (ret == -1) error("Writing the socket");

    	free(aux->username);
    	free(aux->client_addr);
		
		aux=aux->next;
	}

	//Free all UserListItem blocks
	aux=user_list->head;
	while(aux){
		UserListItem* item=aux;
		aux=aux->next;
		free(item);
	}

	//Free all resources and close fd
	free(user_list);
	free(list_users);
	free(struct_msg);
	free(struct_msg_send);
	free(args);

	int ret=close(fd_socket_udp);
	if(ret==-1) error("Closing the socket");
	ret=close(fd_socket_udp_h);
	if(ret==-1) error("Closing the socket");

	exit(EXIT_SUCCESS);
}

//Thread that handle the login of a new user
void* accept_handler(void* args){
	thread_acc_arg* arg=(thread_acc_arg*)args;

	int fd_conn=arg->fd_tcp;
	int fd_socket_udp_h=arg->fd_udp_h;

	AddUserV(fd_conn,fd_socket_udp_h);
		
	UserListPrint(user_list);

	/*int ret=close(fd_conn);
	if(ret==-1) error("Closing the connection TCP");*/

	int tcp_close=1;

	while(tcp_close!=0){

		tcp_close=HandleVocalMessages(fd_conn,"prova");
	}

	free(args);
	int ret=close(fd_conn);
	if(ret==-1) error("Closing TCP");

	pthread_exit(NULL);
}

//Thread that handle the routing of messages
void* handler(void* args){

	thread_args* arg=(thread_args*)args;

	int fd=arg->fd;
	int fd_h=arg->fd_h;

	while(1){
		HandlingMessages(fd,fd_h);
	}

	pthread_exit(NULL);
}

void create_list_users_online(int fd){
	int ret;
	UserListItem* item=user_list->head;
	char* only="You are the only user online :)";
	while(item!=NULL){
		memset(list_users,0,sizeof(list_users));
		struct sockaddr_in* client=item->client_addr;
		int fromlen=sizeof(*client);
		if(user_list->count==1){
			strcpy(list_users,only);
			strcpy(struct_msg_send->name,"list");
			strcpy(struct_msg_send->txt,list_users);
			ret=sendto(fd,struct_msg_send,sizeof(msg),0,(struct sockaddr*)client,fromlen);
			if(ret==-1) error("sending messages");
			break;
		}
		char* name=item->username;
		UserListItem* aux=user_list->head;
		while(aux!=NULL){
			if(strcmp(name,aux->username)!=0){
				strcat(list_users,aux->username);
				strcat(list_users,"-");
			}
			aux=aux->next;
		}
		//printf("%s\n",list_users);
		strcpy(struct_msg_send->name,"list");
		strcpy(struct_msg_send->txt,list_users);

		ret=sendto(fd,struct_msg_send,sizeof(msg),0,(struct sockaddr*)client,fromlen);
		if(ret==-1) error("sending messages");

		item=item->next;
	}
}


int Check_password(char* name,char* password){

	char aux_password[1024];

	char folder[1024]="data_users/";

	int len_password=strlen(password);
	
	char* aux_name=malloc(1024);

	strcpy(aux_name,name);

	strcat(aux_name,".txt");

	strcat(folder,aux_name);

	//if a file .txt with the name of the user doesn't exit return -1
	int fd=open(folder,O_RDONLY | O_EXCL);
	if(fd==-1){
		free(aux_name);
		return -1;
	}

	//Else read from the file the password 
	int res=read(fd,aux_password,len_password);
	if(res==-1) error("Reading from the file");

	res=close(fd);
	if(res==-1) error("Closing the file");

	free(aux_name);

	//Check if the password is correct
	return strcmp(password,aux_password);
}

void Data_user(char* name,char* password){

	char folder[1024]="data_users/";

	char* aux_name=malloc(1024);

	strcpy(aux_name,name);

	strcat(aux_name,".txt");

	strcat(folder,aux_name);

	int len=strlen(password);

	int fd=open(folder,O_WRONLY | O_CREAT,0660);
	if(fd==-1) error("Creating the new file");

	int res_wr=write(fd,password,len);
	if(res_wr==-1) error("Writing in the file");

	int ret=close(fd);
	if(ret==-1) error("Closing the file");

	free(aux_name);

	return;

}

void Remove_user(char* name,int fd){
	char* user_removed=List_delete(user_list,name);
	if(user_removed!=NULL){
		printf("USER [%s] HAS LEFT THE CHAT\n",user_removed);
		//sending_list_users_online(fd);
		create_list_users_online(fd);
	}
}

void AddUserV(int fd_conn_tcp,int fd_conn_udp_h){

	int error_msg_len=strlen(ERROR);

	int ok_msg_len=strlen(OK);

	int ret;

	struct sockaddr_in client={0};

	char username[1024];
	memset(username, 0,sizeof(username));

	char password[1024];
	memset(password, 0,sizeof(password));

	char msg_udp[1024];
	memset(msg_udp,0,sizeof(msg_udp));

	socklen_t len_struct=sizeof(struct sockaddr_in);

	recevitor_messagesTCP(fd_conn_tcp,username);

	recevitor_messagesTCP(fd_conn_tcp,password);

	if(strlen(username)==0 || strlen(password)==0)
		return;

	//First of all check if the user is online now
	if(List_find_username(user_list,username)){
			//If a user with the same name is online send ERROR
			sending_messagesTCP(fd_conn_tcp,ERROR);
	}else{

		//Check_password return -1 if the user is a new user
		//return 0 if the password is correct
		//return something else if is not correct
    	int check=Check_password(username,password);

    	if(check==-1){

    		//If the user is new create a file .txt with his password
    		Data_user(username,password);

    		sending_messagesTCP(fd_conn_tcp,OK);

    		//Receive the fake msg from user to save his struct
    		ret=recvfrom(fd_conn_udp_h,msg_udp,1024,0,(struct sockaddr*)&client,&len_struct);
			if (ret == -1) error("Reading from the socket");

			UserListItem* new_user=malloc(sizeof(UserListItem));
			if(new_user==NULL) error("Allocate memory");
			new_user->username=malloc(1024);
			if(new_user->username==NULL) error("Allocate memory");
			new_user->client_addr=malloc(sizeof(struct sockaddr_in));
			if(new_user->client_addr==NULL) error("Allocate memory");
			new_user->fd=fd_conn_tcp;

	    	List_init_node(username,&client,new_user);

	    	List_insert(user_list,new_user);

	    	printf("~~~~~~~~~NEW USER ADDED~~~~~~~~~~~\n");

	    	//Every time a new user is online send the new list of users to all
	    	//sending_list_users_online(fd_conn_udp_h);
	    	create_list_users_online(fd_conn_udp_h);


    	}
    	else if(check==0){

    		sending_messagesTCP(fd_conn_tcp,OK);

    		//Receive the fake msg from user to save his struct
    		ret=recvfrom(fd_conn_udp_h,msg_udp,1024,0,(struct sockaddr*)&client,&len_struct);
			if (ret == -1) error("Reading from the socket");

			UserListItem* new_user=malloc(sizeof(UserListItem));
			if(new_user==NULL) error("Allocate memory");
			new_user->username=malloc(1024);
			if(new_user->username==NULL) error("Allocate memory");
			new_user->client_addr=malloc(sizeof(struct sockaddr_in));
			if(new_user->client_addr==NULL) error("Allocate memory");
			new_user->fd=fd_conn_tcp;

	    	List_init_node(username,&client,new_user);

	    	List_insert(user_list,new_user);

	    	printf("~~~~~~~~~NEW USER ADDED~~~~~~~~~~~\n");

	    	//Every time a new user is online send the new list of users to all
	    	//sending_list_users_online(fd_conn_udp_h);
	    	create_list_users_online(fd_conn_udp_h);

	    }
	    else{
	    	sending_messagesTCP(fd_conn_tcp,ERROR_PASS);
	    }

	}

}

void sending_messagesTCP(int fd_conn,char* msg){

	int msg_len=strlen(msg);
	int bytes_send=0;

	int ret;

	//Deal with partially send messages
	while(bytes_send<msg_len){
		ret = send(fd_conn, msg + bytes_send, msg_len - bytes_send,0);
		if (ret == -1 && errno == EINTR) continue;
    	if (ret == -1) error("Writing the socket");
    	bytes_send += ret;
	}

}

void recevitor_messagesTCP(int fd_conn,char* msg){
	
	int ret;

	int bytes_recvs=0;

	//Deal with partially recv messages
	do{
		ret=recv(fd_conn,msg+bytes_recvs,1,0);
		if (ret == -1 && errno == EINTR) continue;
        	if (ret == -1) error("Reading from the socket");
        	if (ret == 0) break;
		bytes_recvs++;
	}while(msg[bytes_recvs-1]!='\n');

	msg[bytes_recvs-1]='\0';

}

int setUpConnectionTCP(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_STREAM,0);
	if(fd==-1) error("Creating a new socket");

	int reuseaddr_opt = 1;
    res = setsockopt(fd,SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt, sizeof(reuseaddr_opt));
    if (res==-1) error("Cannot set SO_REUSEADDR option");

	res=bind(fd,(struct sockaddr*) server_struct,sockaddr_len);
	if(res==-1) error(strerror(errno));

	res=listen(fd,CONN_QUEUE);
	if(res==-1) error("Listening");

	return fd;
}

int setUpConnectionUDP(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Creating a new socket");

	int reuseaddr_opt = 1;
    res = setsockopt(fd,SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt, sizeof(reuseaddr_opt));
    if (res==-1) error("Cannot set SO_REUSEADDR option");

	res=bind(fd,(struct sockaddr*)server_struct,sockaddr_len);
	if(res==-1) error(strerror(errno));

	return fd;

}

void HandlingMessages(int fd_conn,int fd_conn_h){

	char* quit_command=QUIT;
	
	int quit_command_len=strlen(quit_command);

	char* user_error=USER_ERROR;

	int user_error_len=strlen(user_error);

	char* not_online_error=NOT_ONLINE_ERROR;

	int not_online_error_len=strlen(not_online_error);

	int ret;

	char client_msg[1024];
	char name_src[1024];

	memset(client_msg,0,1024);
	memset(name_src,0,1024);

	struct sockaddr_in client={0};
	socklen_t len_struct=sizeof(struct sockaddr_in);

	int bytes_recvs=0;

	//Receve a struct with the name of the sender and the message
	ret=recvfrom(fd_conn,struct_msg,2048,0,(struct sockaddr*)&client,&len_struct);
	if (ret == -1) error("Reading from the socket");

	strcpy(client_msg,struct_msg->txt);
	strcpy(name_src,struct_msg->name);

	//If a user send 'Quit' to the server remove him from the list
	if(memcmp(client_msg,QUIT,quit_command_len)==0){
		Remove_user(name_src,fd_conn_h);
		return;
	}


	int name_len=strlen(name_src);

	//Messages are in from "Addressee-message"
	char* dst=strtok(client_msg,"-");
	char* txt=strtok(NULL,"");

	char* broad="All";

	//If you want to send a broadcast
	if(strcmp(dst,broad)==0){

		printf("SENDING MESSAGE FROM [%s] TO [%s] \n",name_src,dst);

		UserListItem* aux=user_list->head;
		while(aux){
			if(strcmp(aux->username,name_src)!=0){


				int fromlen=sizeof(*(aux->client_addr));

				strcpy(struct_msg_send->name,name_src);
				strcpy(struct_msg_send->txt,txt);

				ret = sendto(fd_conn_h,struct_msg_send,sizeof(msg),0,(struct sockaddr *)aux->client_addr,fromlen);
		    	if (ret == -1) error("Writing the socket");

			}
			aux=aux->next;
		}
		return;
	}

	//Check if the addressee is online
	struct sockaddr_in* client_dst=Find_client_struct(user_list,dst);

	if(client_dst!=NULL){

		int fromlen=sizeof(*client_dst);

		if(strcmp(name_src,Find_client_name(user_list,client_dst))==0){
			strcpy(struct_msg_send->name,user_error);
			ret = sendto(fd_conn_h,struct_msg_send,sizeof(msg),0,(struct sockaddr *)client_dst,fromlen);
    		if (ret == -1) error("Writing the socket");
    		return;
		}

		printf("SENDING MESSAGE FROM [%s] TO [%s] \n",name_src,dst);

		strcpy(struct_msg_send->name,name_src);
		strcpy(struct_msg_send->txt,txt);

		ret = sendto(fd_conn_h,struct_msg_send,sizeof(msg),0,(struct sockaddr *)client_dst,fromlen);
    	if (ret == -1) error("Writing the socket");

    }else{

    	client_dst=Find_client_struct(user_list,name_src);

    	int fromlen=sizeof(*client_dst);
    	strcpy(struct_msg_send->name,not_online_error);
    	
    	ret = sendto(fd_conn_h,struct_msg_send,sizeof(msg),0,(struct sockaddr *)client_dst,fromlen);
    	if (ret == -1) error("Writing the socket");

    	return;
    }

}



int main(int argc, char** argv){

	//Allocate resources
	list_users=malloc(1024);
	if(list_users==NULL) error("Allocate memory");
	struct_msg=malloc(sizeof(msg));
	if(struct_msg==NULL) error("Allocate memory");
	struct_msg_send=malloc(sizeof(msg));
	if(struct_msg_send==NULL) error("Allocate memory");
	memset(struct_msg_send,0,sizeof(msg));

	int ret;
	pthread_t thread_accept;

	user_list=malloc(sizeof(UserHead));
	if(user_list==NULL) error("Allocate memory");

	user_list->head=NULL;
	user_list->count=0;

	struct sigaction act={0};
	act.sa_handler=sig_handler;


	//Install handler of signals
	ret=sigaction(SIGINT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGQUIT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTERM,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTSTP,&act,NULL);
	if(ret==-1) error("sigaction");
	
	struct sockaddr_in server_struct_tcp = {0};
	struct sockaddr_in server_struct_udp = {0};
	struct sockaddr_in server_struct_udp_h = {0};

	int sockaddr_len = sizeof(struct sockaddr_in);
	
	//We use only IPv4
	server_struct_tcp.sin_family=AF_INET;
 
    //To accept connections from any
	server_struct_tcp.sin_addr.s_addr=INADDR_ANY;

	//htons() to convert from host byte order to network byte order
	server_struct_tcp.sin_port=htons(NUM_DOOR_TCP);

	server_struct_udp_h.sin_family=AF_INET;
	server_struct_udp_h.sin_addr.s_addr=INADDR_ANY;
	server_struct_udp_h.sin_port=htons(NUM_DOOR_UDP_HAND);


	server_struct_udp.sin_family=AF_INET;
	server_struct_udp.sin_addr.s_addr=INADDR_ANY;
	server_struct_udp.sin_port=htons(NUM_DOOR_UDP);


	int fd_socket_tcp=setUpConnectionTCP(&server_struct_tcp,sockaddr_len);

	struct sockaddr_in client_struct = {0};

	fd_socket_udp=setUpConnectionUDP(&server_struct_udp,sockaddr_len);
	fd_socket_udp_h=setUpConnectionUDP(&server_struct_udp_h,sockaddr_len);

	args=malloc(sizeof(thread_args));
	if(args==NULL) error("Allocate memory");

	args->fd_h=fd_socket_udp_h;
	args->fd=fd_socket_udp;

	//Thread to handle the routing of messages
	ret=pthread_create(&thread,NULL,handler,(void*)args);
	if(ret==-1) error("Create thread");

	while(1){

		memset(&client_struct, 0, sizeof(struct sockaddr_in));

		//accept new connections
		int fd_conn=accept(fd_socket_tcp,(struct sockaddr*)&client_struct,(socklen_t *)&sockaddr_len);
		if(fd_conn==-1) error("Accepting new connections");

		printf("---------NEW CONNECTION---------\n");

		thread_acc_arg* accept_struct=malloc(sizeof(thread_acc_arg));
		if(accept_struct==NULL) error("Allocate memory");

		accept_struct->fd_tcp=fd_conn;
		accept_struct->fd_udp_h=fd_socket_udp_h;

		//Create thread that handle the login of each user
		ret=pthread_create(&thread_accept,NULL,accept_handler,(void*)accept_struct);
		if(ret==-1) error("Create thread accept");

		ret=pthread_detach(thread_accept);
		if(ret!=0) error("Detach");


	}

	return EXIT_SUCCESS;
}
