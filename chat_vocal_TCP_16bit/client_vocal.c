#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <alsa/asoundlib.h>

#include "client.h"


typedef struct msg_vocal{
	char addressee[1024];
	char sender[1024];
}msg_vocal;

//Alsa variables
snd_pcm_t* rec;
snd_pcm_t* play;
snd_pcm_stream_t stream;
snd_pcm_uframes_t frames;
int size;
char *buffer;

int check_server;
msg_vocal* struct_vocal;
msg_vocal* struct_vocal_recv;
time_t rawtime;
struct tm * timeinfo;
int fd_conn_udp_h;
int fd_conn_udp;
int fd_conn_tcp;
struct sockaddr_in server_struct_udp_h;
struct sockaddr_in server_struct_udp;
msg* struct_msg;
msg* struct_msg_recv;

pthread_t thread;
pthread_t thread2;
char server_ip[16];

//Handler of signalsa
void sig_handler(int sig){

	pthread_cancel(thread);
	pthread_join(thread,NULL);

	pthread_cancel(thread2);
	pthread_join(thread2,NULL);

	//Tell that the client is leaving the chat
	if(check_server!=0){

		int fromlen=sizeof(server_struct_udp);

		strcpy(struct_msg->txt,QUIT);

		//Send msg to the server
		int ret = sendto(fd_conn_udp,struct_msg,2048,0,(struct sockaddr *)&server_struct_udp,fromlen);
		if (ret == -1) error(strerror(errno));
	}


	//Free all resources and close fd
	free(struct_msg);
	free(struct_msg_recv);
	free(struct_vocal);
	free(struct_vocal_recv);
	free(buffer);

	int ret=close(fd_conn_udp);
	if(ret==-1) error("Closing a socket");
	ret=close(fd_conn_udp_h);
	if(ret==-1) error("Closing a socket");
	ret=close(fd_conn_tcp);
	if(ret==-1) error("Closing a socket");

	snd_pcm_drain(play);
    snd_pcm_close(play);

    snd_config_update_free_global();

	exit(EXIT_SUCCESS);
}

//Error handler
void error(char* msg){
	printf("Error in %s\n",msg);
	exit(EXIT_FAILURE);
}

int kbhit(){

    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv);
}

int getch(){

    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}

int recvvocalmessages(int fd_tcp,int size){

	char txt[1024];
	memset(txt,0,sizeof(txt));

	int ret;

	int audio_size;

	ret=recv(fd_tcp,&audio_size,sizeof(audio_size),0);
	if(ret==-1) error(strerror(errno));
	if(ret==0) return 0;

	//printf("audio_size: %d\n",audio_size);

	char buffer_audio[audio_size];
	memset(buffer_audio,0,audio_size);

	ret=recv(fd_tcp,struct_vocal_recv,2048,0);
	if(ret==-1) error(strerror(errno));

	//printf("Sender %s \n",struct_vocal_recv->sender);

	int recv_bytes=0;

	while(recv_bytes<audio_size){
		ret=recv(fd_tcp,buffer_audio+recv_bytes,audio_size-recv_bytes,0);
		if(ret==-1) error(strerror(errno));
		recv_bytes+=ret;
	}

	printf("MSG VOCAL OF [%ld] bytes RECV FROM [%s]\n",sizeof(buffer_audio),struct_vocal_recv->sender);

	int fd_file=open("audio_client.raw",O_CREAT | O_TRUNC | O_RDWR,0666);
	if(fd_file==-1) error(strerror(errno));

	int written_bytes=0;

	while(written_bytes<audio_size){
		ret=write(fd_file,buffer_audio+written_bytes,audio_size-written_bytes);
		if(ret==-1) error(strerror(errno));
		written_bytes+=ret;
	}
	//printf("File written\n");

	//printf("Vocal message receive from |%s|,do you want to listen?[y/n]:\n",struct_vocal_recv->sender);
	//fflush(stdout);

	//if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

	//int msg_len=strlen(txt);
	
	//txt[msg_len - 1] = '\0';

	//if(strcmp(txt,"y")==0){

	lseek(fd_file,0,SEEK_SET);

	memset(txt,0,sizeof(txt));

	int rc=1;

	memset(buffer_audio,0,sizeof(buffer_audio));

	/*while (rc!=0) {
        rc = read(fd_file,buffer_audio, size);
        if (rc == -1) error("Read from file");
        //printf("Riproducing...  %d,frames %ld\n",rc,frames);
        rc = snd_pcm_writei(play,buffer_audio, frames);
        if (rc == -1) error("Playback vocal message");
        //printf("writei %d\n",rc);
        //sleep(1);
    }*/


    while (rc!=0) {
      	rc = read(fd_file, buffer_audio, size);
      	if (rc == 0) {
        	fprintf(stderr, "end of file on input\n");
        	break;
      	} else if (rc != size) {
        	fprintf(stderr,
              "short read: read %d bytes\n", rc);
      	}
      	rc = snd_pcm_writei(play, buffer_audio, frames);
      	if (rc == -EPIPE) {
        	//EPIPE means underrun 
        	//fprintf(stderr, "underrun occurred\n");
        	snd_pcm_prepare(play);
      	} else if (rc < 0) {
        	fprintf(stderr,
              "error from writei: %s\n",
              snd_strerror(rc));
      	}  else if (rc != (int)frames) {
        	fprintf(stderr,
              "short write, write %d frames\n", rc);
    	}
    }

    rc=close(fd_file);
    if(ret==-1) error("Closing FD");

    return 1;

	//}
}

void sendmessages(int fd_conn,int fd_conn_tcp,struct sockaddr_in* server_struct){

	//Command to close the program
	char* quit_command=QUIT;
	int quit_command_len=strlen(quit_command);

	int ret;
	int finish=1;

	char* aux;
	char txt[1024];
	memset(txt,0,sizeof(txt));

	if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

	int msg_len=strlen(txt);
	
	txt[msg_len - 1] = '\0';

	if(strcmp(txt,"vocal")!=0){

		//printf("Inserisci testo :\n");

		//Read from shell
		//if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

		//If 'Quit' is written so send it to server and close the program
		finish=memcmp(txt,quit_command,quit_command_len);

		aux=strchr(txt,'-');
		if(aux!=NULL && strlen(aux)>1  && check_server!=0){

			strcpy(struct_msg->txt,txt);

			int fromlen=sizeof(*server_struct);

			//Sendo msg to the server
			ret = sendto(fd_conn,struct_msg,2048,0,(struct sockaddr *)server_struct,fromlen);
			if (ret == -1) error(strerror(errno));
		}
		//Chekc if 'Quit' is written
		else if(finish==0){

			//Check if the Server is still open
			if(check_server!=0){

				strcpy(struct_msg->txt,txt);

				int fromlen=sizeof(*server_struct);

				//Sendo msg to the server
				ret = sendto(fd_conn,struct_msg,2048,0,(struct sockaddr *)server_struct,fromlen);
				if (ret == -1) error(strerror(errno));

			}
			
			//Free all resources and close fd
			free(struct_msg);
			free(struct_msg_recv);

			pthread_cancel(thread);
			pthread_join(thread,NULL);

			ret=close(fd_conn_udp);
			if(ret==-1) error("Closing a socket");
			ret=close(fd_conn_udp_h);
			if(ret==-1) error("Closing a socket");

			exit(EXIT_SUCCESS);

		}else if(check_server!=0){
			printf("Message form is 'Addressee-Messagge' \n");
		}else{
			printf("Server is closed,please quit the program\n");
		}

	}else{

		printf("\nPress enter to start recording");
    	int k=getchar();

    	setAlsaParam(1,SND_PCM_STREAM_CAPTURE);

		int fd_file=Recording(rec,frames,buffer,size);
		struct stat st;
  		stat("audio.raw", &st);
  		int audio_len=st.st_size;

  		lseek(fd_file,0,SEEK_SET);

  		char buf_audio[audio_len];
  		memset(buf_audio,0,audio_len);

  		int count=0;

  		//First of all send the length of the vocal file
  		ret=send(fd_conn_tcp,&audio_len,sizeof(audio_len),0);
  		if(ret==-1) error("Sending TCP");

  		int bytes_read=0;

  		while(bytes_read<audio_len){
	  		//Read from the file and fill the buf_audio
	    	ret=read(fd_file,buf_audio+bytes_read,audio_len-bytes_read);
	    	if(ret==-1) error("Read from file");
	    	//printf("%d\n",ret );
	    	bytes_read+=ret;
	    }

	    //printf("size: %ld\n",sizeof(buf_audio));

  		printf("Insert the addressee of the vocal: ");

  		memset(txt,0,1024);

  		if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

  		int msg_len=strlen(txt);
	
		txt[msg_len - 1] = '\0';

		strcpy(struct_vocal->addressee,txt);

		ret=send(fd_conn_tcp,struct_vocal,2048,0);
		if(ret==-1) error("Sending TCP");

		ret=send(fd_conn_tcp,buf_audio,audio_len,0);
		if(ret==-1) error("Sending TCP");

		ret=close(fd_file);
		if(ret==-1) error("Closing FD");

		snd_pcm_drain(rec);
    	snd_pcm_close(rec);

	}

}

int Recording(snd_pcm_t* handle,snd_pcm_uframes_t frames,char* buffer,int size){

	int rc;

	//printf("%x\n",handle);

    int fd=open("audio.raw",O_TRUNC | O_CREAT | O_RDWR,0666);
    if(fd==-1) error("Open file audio");

    frames=32;

    memset(buffer,0,sizeof(buffer));

    printf("Press any key to stop recording\n");

    while (!kbhit()) {
      //loops--;
      rc = snd_pcm_readi(handle, buffer, frames);
      if (rc == -EPIPE) {
        //EPIPE means overrun 
        //fprintf("overrun occurred\n");
        snd_pcm_prepare(handle);
      } else 
      if (rc < 0) error("Error from read");

      rc = write(fd, buffer, size);
      if (rc ==-1) error("Write error");
    }
    (void)getch();

    return fd;
}

void setAlsaParam(int flag,snd_pcm_stream_t stream){

    snd_pcm_hw_params_t *params;
	unsigned int val;
	int dir;

	if(flag==1){

		//Open PCM device
		int rc = snd_pcm_open(&rec,"default",stream, 0);
		if (rc < 0) error("Unable to open pcm device");

		//Allocate a hardware parameters object
		snd_pcm_hw_params_alloca(&params);

		//Fill it in with default values.
		snd_pcm_hw_params_any(rec, params);

		//Set the desired hardware parameters

		//Interleaved mode
		snd_pcm_hw_params_set_access(rec, params,SND_PCM_ACCESS_RW_INTERLEAVED);

		//Signed 16-bit little-endian format
		snd_pcm_hw_params_set_format(rec, params,SND_PCM_FORMAT_S16_LE);

		// Two channels (stereo)
		snd_pcm_hw_params_set_channels(rec, params, 2);

		//44100 bits/second sampling rate (CD quality)
		val = 44100;
		snd_pcm_hw_params_set_rate_near(rec, params,&val, &dir);

		//Set period size to 32 frames.
		frames = 32;
		snd_pcm_hw_params_set_period_size_near(rec,params, &frames, &dir);

		// Write the parameters to the driver
		rc = snd_pcm_hw_params(rec, params);
		if (rc < 0) error("Unable to set hw parameters");

		// Use a buffer large enough to hold one period
		snd_pcm_hw_params_get_period_size(params,&frames, &dir);

	    size = frames * 4; //2 bytes/sample, 2 channels 
		buffer = (char *) malloc(size);

		//We want to loop for 5 seconds 
		snd_pcm_hw_params_get_period_time(params,&val, &dir);

	}else{

		//Open PCM device
		int rc = snd_pcm_open(&play,"default",stream, 0);
		if (rc < 0) error("Unable to open pcm device");

		//Allocate a hardware parameters object
		snd_pcm_hw_params_alloca(&params);

		//Fill it in with default values.
		snd_pcm_hw_params_any(play, params);

		//Set the desired hardware parameters

		//Interleaved mode
		snd_pcm_hw_params_set_access(play, params,SND_PCM_ACCESS_RW_INTERLEAVED);

		//Signed 16-bit little-endian format
		snd_pcm_hw_params_set_format(play, params,SND_PCM_FORMAT_S16_LE);

		// Two channels (stereo)
		snd_pcm_hw_params_set_channels(play, params, 2);

		//44100 bits/second sampling rate (CD quality)
		val = 44100;
		snd_pcm_hw_params_set_rate_near(play, params,&val, &dir);

		//Set period size to 32 frames.
		frames = 32;
		snd_pcm_hw_params_set_period_size_near(play,params, &frames, &dir);

		// Write the parameters to the driver
		rc = snd_pcm_hw_params(play, params);
		if (rc < 0) error("Unable to set hw parameters");

		// Use a buffer large enough to hold one period
		snd_pcm_hw_params_get_period_size(params,&frames, &dir);

	    size = frames * 4; //2 bytes/sample, 2 channels 
		//buffer = (char *) malloc(size);

		//We want to loop for 5 seconds 
		snd_pcm_hw_params_get_period_time(params,&val, &dir);

	}
}

//Thread for recv messages
void* handler(void* args){
		
	while(1){
		recevitor_messagesUDP(fd_conn_udp_h,&server_struct_udp_h);
	}
	pthread_exit(NULL);
}

//Thread for recv vocal messages
void* handler_vocal(void* args){

	int tcp_close=1;
		
	while(tcp_close!=0){
		tcp_close=recvvocalmessages(fd_conn_tcp,size);
	}
	pthread_exit(NULL);
}

void sending_messagesUDP(int fd_conn,struct sockaddr_in* server_struct){

	//Command to close the program
	char* quit_command=QUIT;
	int quit_command_len=strlen(quit_command);

	int ret;
	int finish=1;

	char* aux;
	char txt[1024];
	memset(txt,0,sizeof(txt));

	//Read from shell
	if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

	int msg_len=strlen(txt);
	
	txt[msg_len - 1] = '\0';

	//If 'Quit' is written so send it to server and close the program
	finish=memcmp(txt,quit_command,quit_command_len);

	aux=strchr(txt,'-');
	if(aux!=NULL && strlen(aux)>1  && check_server!=0){

		strcpy(struct_msg->txt,txt);

		int fromlen=sizeof(*server_struct);

		//Sendo msg to the server
		ret = sendto(fd_conn,struct_msg,2048,0,(struct sockaddr *)server_struct,fromlen);
		if (ret == -1) error(strerror(errno));
	}
	//Chekc if 'Quit' is written
	else if(finish==0){

		//Check if the Server is still open
		if(check_server!=0){

			strcpy(struct_msg->txt,txt);

			int fromlen=sizeof(*server_struct);

			//Sendo msg to the server
			ret = sendto(fd_conn,struct_msg,2048,0,(struct sockaddr *)server_struct,fromlen);
			if (ret == -1) error(strerror(errno));

		}
		
		//Free all resources and close fd
		free(struct_msg);
		free(struct_msg_recv);

		pthread_cancel(thread);
		pthread_join(thread,NULL);

		ret=close(fd_conn_udp);
		if(ret==-1) error("Closing a socket");
		ret=close(fd_conn_udp_h);
		if(ret==-1) error("Closing a socket");
		ret=close(fd_conn_tcp);
		if(ret==-1) error("Closing a socket");

		snd_pcm_drain(play);
    	snd_pcm_close(play);

    	snd_config_update_free_global();

		exit(EXIT_SUCCESS);

	}else if(check_server!=0){
		printf("Message form is 'Addressee-Messagge' \n");
	}else{
		printf("Server is closed,please quit the program\n");
	}

}

void sending_messagesTCP(int fd_conn,char* msg){

	int msg_len=strlen(msg);
	int bytes_send=0;

	int ret;

	//deal with partially sent messages
	while(bytes_send<msg_len){
		ret = send(fd_conn, msg + bytes_send, msg_len - bytes_send,0);
		if (ret == -1 && errno == EINTR) continue;
    	if (ret == -1) error("Writing the socket");
    	bytes_send += ret;
	}

}

void recevitor_messagesUDP(int fd_conn,struct sockaddr_in* server_struct){
	
	int ret;
	char* user;

	socklen_t len_struct=sizeof(struct sockaddr_in);

	ret=recvfrom(fd_conn,struct_msg_recv,sizeof(msg),0,NULL,NULL);
	if (ret == -1) error("Reading from the socket");
	
	//Check if message is from the server that tells the list of users
	if(strcmp(struct_msg_recv->name,"list")==0){
		printf("\nUSERS ONLINE:[");
		user=strtok(struct_msg_recv->txt,"-");
		while(user){
			printf("|%s|",user);
			user=strtok(NULL,"-");
		}
		printf("]\n");
	}else if(strcmp(struct_msg_recv->name,SERVER_CLOSE)==0){

		printf("%s,please quit the program\n",struct_msg_recv->name);
		check_server=0;	

		pthread_exit(NULL);
	}
	//if the server is sending an error message 
	else if(strcmp(struct_msg_recv->name,USER_ERROR)==0){
		printf("%s\n",struct_msg_recv->name);
	}else if(strcmp(struct_msg_recv->name,NOT_ONLINE_ERROR)==0){
		printf("%s\n",struct_msg_recv->name);

	//Or if the server is sending a message from another user
	}else{

		//Print the time
		time( &rawtime );
		timeinfo = localtime( &rawtime );

		printf("\n[%02d:%02d:%02d-%s]:%s\n",timeinfo->tm_hour, timeinfo->tm_min,timeinfo->tm_sec,struct_msg_recv->name,struct_msg_recv->txt);
	}


}

void recevitor_messagesTCP(int fd_conn,char* msg){
	
	int ret;

	int bytes_recvs=0;

	//Deal with partially recv messages
	do{
		ret=recv(fd_conn,msg+bytes_recvs,1,0);
		if (ret == -1 && errno == EINTR) continue;
        	if (ret == -1) error("Reading from the socket");
        	if (ret == 0) break;
		bytes_recvs++;
	}while(msg[bytes_recvs-1]!='\n');

	msg[bytes_recvs-1]='\0';

}

int setUpConnectionTCP(struct sockaddr_in *server_struct,int sockaddr_len){

    int ret;

    int fd_conn=socket(AF_INET, SOCK_STREAM, 0);
    if(fd_conn==-1) error("Creating the socket");

    ret = connect(fd_conn, (struct sockaddr*) server_struct, sockaddr_len);
    if(ret==-1) error("Connecting to the server"); 

    return fd_conn;
}

void Login(int fd_conn,int fd_conn_udp,struct sockaddr_in *server_struct){

	int ret;

	char password[1024];
	char username[1024];
	char result[1024];
	int login_result;

    memset(password,0,sizeof(password));
  	memset(username,0,sizeof(username));
  	memset(result,0,sizeof(result));
  	

	printf("Insert username: ");

	if(fgets(username, sizeof(username), stdin)!=(char*)username) error("Reading from shell");

	int msg_username=strlen(username);
	
	username[msg_username - 1] = '\n';

	sending_messagesTCP(fd_conn,username);

	printf("Insert password: ");

	if(fgets(password, sizeof(password), stdin)!=(char*)password) error("Reading from shell");

	int msg_password=strlen(password);

	password[msg_password - 1] = '\n';

	sending_messagesTCP(fd_conn,password);

	int fromlen=sizeof(*server_struct);

	recevitor_messagesTCP(fd_conn,result);

	printf("%s\n",result);

	//If the Login is OK send a fake msg to allow the server to save struct of the client
	if((strcmp(result,ERROR)!=0 && strcmp(result,ERROR_PASS)!=0)){
		username[msg_username - 1] = '\0';
		strcpy(struct_msg->name,username);
		strcpy(struct_vocal->sender,username);
		ret = sendto(fd_conn_udp," ",1024,0,(struct sockaddr *)server_struct,fromlen);
		if (ret == -1) error(strerror(errno));
		return;
	}
	else{
		free(struct_msg);
		free(struct_msg_recv);
		exit(EXIT_FAILURE);
	}


}

int setUpConnectionUDP(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	server_struct->sin_family =AF_INET;                    
    server_struct->sin_addr.s_addr = inet_addr(server_ip);    
    server_struct->sin_port = htons(NUM_DOOR_UDP);

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Creating a new socket");

	return fd;

}

int setUpConnectionUDP_H(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	server_struct->sin_family =AF_INET;                    
    server_struct->sin_addr.s_addr = inet_addr(server_ip);    
    server_struct->sin_port = htons(NUM_DOOR_UDP_H);

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Creating a new socket");

	return fd;

}

int main(int argc, char** argv){

	int ret;
	check_server=0;

	struct_msg=malloc(sizeof(msg));
	if(struct_msg==NULL) error("Allocate memory");
	memset(struct_msg,0,sizeof(msg));

	struct_msg_recv=malloc(sizeof(msg));
	if(struct_msg_recv==NULL) error("Allocate memory");
	memset(struct_msg_recv,0,sizeof(msg));

	struct_vocal=malloc(sizeof(msg_vocal));
	if(struct_vocal==NULL) error("Allocate memory");
	memset(struct_vocal,0,sizeof(msg_vocal));

	struct_vocal_recv=malloc(sizeof(msg_vocal));
	if(struct_vocal_recv==NULL) error("Allocate memory");
	memset(struct_vocal_recv,0,sizeof(msg_vocal));

	setAlsaParam(0,SND_PCM_STREAM_PLAYBACK);

	strcpy(server_ip,ADDR_SERVER);

	if(argc==2){
		strcpy(server_ip,argv[1]);
	}

	struct sigaction act={0};
	act.sa_handler=sig_handler;

	//Install handler of signals
	ret=sigaction(SIGINT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGQUIT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTERM,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTSTP,&act,NULL);
	if(ret==-1) error("sigaction");

    struct sockaddr_in server_struct_tcp = {0};
    memset(&server_struct_udp,0,sizeof(struct sockaddr_in));

    int sockaddr_len = sizeof(struct sockaddr_in);

    memset(&server_struct_udp,0,sizeof(struct sockaddr_in));

    //Insert the address of the server
    server_struct_tcp.sin_addr.s_addr = inet_addr(server_ip);

    //Only IPv4
    server_struct_tcp.sin_family = AF_INET;

    //To convert host byte order
    server_struct_tcp.sin_port = htons(NUM_DOOR_TCP); 

    fd_conn_tcp=setUpConnectionTCP(&server_struct_tcp,sockaddr_len);

    fd_conn_udp=setUpConnectionUDP(&server_struct_udp,sockaddr_len);

    fd_conn_udp_h=setUpConnectionUDP_H(&server_struct_udp_h,sockaddr_len);

    Login(fd_conn_tcp,fd_conn_udp_h,&server_struct_udp_h);

    check_server=1;

    //ret=close(fd_conn_tcp);
    //if(ret==-1) error("Closing TCP connection");

    printf("\n");
	printf("Welcome to this chat,the message form must be 'Addressee-Messagge',using 'All' as Addressee you can send a broadcast,if you want to send a vocal message please use the command 'vocal',if you want to leave the chat please send 'Quit' \n");
	printf("\n");

    //Create a thread to send messages
    ret=pthread_create(&thread,NULL,handler,NULL);
    if(ret==-1) error("Creating the thread");

    ret=pthread_create(&thread2,NULL,handler_vocal,NULL);
    if(ret==-1) error("Creating the thread");

    //A while fot receve messages
	while(1){
		//int fd_conn,int fd_conn_tcp,struct sockaddr_in* server_struct,snd_pcm_t* handle,snd_pcm_uframes_t frames,char* buffer,int size
		sendmessages(fd_conn_udp,fd_conn_tcp,&server_struct_udp);
	}

	//printf("debug\n");

	pthread_join(thread,NULL);
	pthread_join(thread2,NULL);


    

	return 0;


}
          