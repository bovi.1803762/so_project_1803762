CC=gcc
CCOPTS=-pthread
CCOPTS2=-lasound

OBJS=server.o\
linked_list.o\
client.o\

HEADERS=linked_list.h\
server.h\
client.h


BINS=server\
client\

.phony: clean all

all: $(BINS)

%.o:	%.c $(HEADERS)
	$(CC) $(CCOPTS) -c -o $@  $<

server:	server.c linked_list.c
	$(CC) $(CCOPTS) -g -o $@ $^ 

client:	client.c
	$(CC) $(CCOPTS) -g -o $@ $^ 

clean:
	rm -rf *.o *~ $(BINS)
	rm -rf data_users/*.txt
