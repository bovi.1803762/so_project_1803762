#define ADDR_SERVER "127.0.0.1"
#define ERROR "This username is already used\n"
#define OK "Registration completed\n"
#define ERROR_PASS "Password wrong\n"
#define USER_ERROR "You cannot send a message to yourself"
#define NOT_ONLINE_ERROR "This user is not online now"
#define QUIT "Quit"
#define SERVER_CLOSE "Server is close"
#define NUM_DOOR_TCP 2902
#define NUM_DOOR_UDP 2903
#define NUM_DOOR_UDP_HAND 2904
#define CONN_QUEUE 5
#define VOCAL 0
#define TEXT 1

typedef struct thread_args{
	int fd;
	int fd_h;
}thread_args;

//Struct for messages
typedef struct msg{
	char name[1024]; //Name of the source
	char data[1024]; //Will contain text or audio buffer
	char addressee[1024];
	int flag;  //To see if is a text or vocal message
	int seq_num; //To use in case of vocal messages
	int len_buf; //Length of buffer
	int audio_len; //Entire len of the audio
}msg;

typedef struct thread_acc_arg{
	int fd_tcp;
	int fd_udp_h;
}thread_acc_arg;

void AddUser(int fd_conn_tcp,int fd_conn_udp);
int error(char* msg);
int setUpConnectionTCP(struct sockaddr_in *server_struct,int sockaddr_len);
int setUpConnectionUDP(struct sockaddr_in *server_struct,int sockaddr_len);
void RecvMessagesTCP(int fd_conn,char* msg);
void SendMessagesTCP(int fd_conn,char* msg);
void HandlingMessages(int fd_conn,int fd_conn_h);
void ListUsersOnline(int fd);
int CheckPassword(char* name,char* password);
void DataUser(char* name,char* password);
void RemoveUser(char* name,int fd);


