#include "linked_list.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>


void List_init(UserHead* list){

	list=malloc(sizeof(UserHead));

	list->head=NULL;
	list->count=0;
}

char* List_delete(UserHead* list,char* name){
	UserListItem* aux=list->head;
	UserListItem* prev;

	if(strcmp(aux->username,name)==0){
		list->head=aux->next;
		free(aux->username);
		free(aux->client_addr);
		free(aux);
		list->count--;
		return name;
	}

	while(aux!=NULL && strcmp(aux->username,name)!=0){
		prev=aux;
		aux=aux->next;
	}

	if(aux==NULL) return NULL;

	prev->next=aux->next;

	free(aux->username);
	free(aux->client_addr);
	free(aux);

	list->count--;

	return name;
}

void List_init_node(char* username,struct sockaddr_in* client_addr,UserListItem* new_node){

	strcpy(new_node->username,username);
	memcpy(new_node->client_addr,client_addr,sizeof(struct sockaddr_in));

	new_node->next=NULL;

	return;
}

char* Find_client_name(UserHead* user_list,struct sockaddr_in* client_addr){
	UserListItem* aux=user_list->head;
	while(aux){
		if (aux->client_addr->sin_port==client_addr->sin_port)
  			return aux->username;
	aux=aux->next;
	}
	return 0;
}

/*int Find_client_fd(UserHead* user_list,char* username){
	UserListItem* aux=user_list->head;
	while(aux){
		if(strcmp(aux->username,username)==0)
			return aux->fd;
	aux=aux->next;
	}
	return 0;
}*/

struct sockaddr_in* Find_client_struct(UserHead* user_list,char* dst){
	UserListItem* aux=user_list->head;
	while(aux){
		if (strcmp(aux->username,dst)==0)
  			return aux->client_addr;
	aux=aux->next;
	}
	return 0;
}

void List_insert(UserHead* list,UserListItem* nodo){

	if(list->head==NULL){
		list->head=nodo;
		list->count++;
		return;
	}

	//Insert in head
	/*UserListItem* tmp=list->head;

	list->head=nodo;

	list->head->next=tmp;*/

	//Insert in tail
	UserListItem* aux=list->head;
	while(aux->next!=NULL)
		aux=aux->next;

	aux->next=nodo;

	list->count++;

}

void UserListPrint(UserHead* list){
  char client_ip[INET_ADDRSTRLEN];	 
  UserListItem* aux=list->head;
  printf("NUM USERS: %d\n",list->count);
  while(aux){
    inet_ntop(AF_INET, &(aux->client_addr->sin_addr), client_ip, INET_ADDRSTRLEN);
    printf("USERNAME:[%s]-IP[%s]-PORT[%hd]\n", aux->username,client_ip,aux->client_addr->sin_port);
    aux=aux->next;
  }
}



//Return one if there is already a user with the same username
int List_find_username(UserHead* list,char* username) {
  // linear scanning of list
  UserListItem* aux=list->head;
  while(aux){
    if (strcmp(aux->username,username)==0)
      return 1;
    aux=aux->next;
  }
  return 0;
}

