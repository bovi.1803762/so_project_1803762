#define ADDR_SERVER "127.0.0.1"
#define QUIT "Quit"
#define ERROR_PASS "Password wrong"
#define ERROR "This username is already used"
#define USER_ERROR "You cannot send a message to yourself"
#define NOT_ONLINE_ERROR "This user is not online now"
#define SERVER_CLOSE "Server is close"
#define NUM_DOOR_TCP 2902
#define NUM_DOOR_UDP 2903
#define NUM_DOOR_UDP_H 2904
#define VOCAL 0
#define TEXT 1

//Struct for messages
typedef struct msg{
	char name[1024]; //Name of the source
	char data[1024]; //Will contain text or audio buffer
	char addressee[1024];
	int flag;  //To see if is a text or vocal message
	int seq_num; //To use in case of vocal messages
	int len_buf; //Length of txt buffer
	int audio_len; //Entire len of the audio
}msg;

int setUpConnectionTCP(struct sockaddr_in *server_struct,int sockaddr_len);
int setUpConnectionUDP(struct sockaddr_in *server_struct,int sockaddr_len);
void error(char* msg);
void Login(int fd_conn,int fd_conn_udp,struct sockaddr_in *server_struct);
void SendMessagesUDP(int fd_conn,struct sockaddr_in* server_struct);
void RecvMessagesUDP(int fd_conn,struct sockaddr_in* server_struct);
void SendMessagesTCP(int fd_conn,char* msg);
void RecvMessagesTCP(int fd_conn,char* msg);
int setUpConnectionUDP_H(struct sockaddr_in *server_struct,int sockaddr_len);

//Vocal
int Recording(snd_pcm_t* handle,snd_pcm_uframes_t frames,char* buffer,int size);
void setAlsaParam(int flag,snd_pcm_stream_t stream);