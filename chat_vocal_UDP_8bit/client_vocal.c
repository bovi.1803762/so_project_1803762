#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <alsa/asoundlib.h>

#include "client.h"

//Alsa variables
snd_pcm_t* rec;
snd_pcm_t* play;
snd_pcm_stream_t stream;
snd_pcm_uframes_t frames;
int size;
char *buffer;

int check_server;
time_t rawtime;
struct tm * timeinfo;
int fd_conn_udp_h;
int fd_conn_udp;
struct sockaddr_in server_struct_udp_h;
struct sockaddr_in server_struct_udp;
msg* struct_msg;
msg* struct_msg_recv;
pthread_t thread;
char server_ip[16];

int kbhit(){

    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv);
}

int getch(){

    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}


int Recording(snd_pcm_t* handle,snd_pcm_uframes_t frames,char* buffer,int size){

	int rc;


	//First of all create a file .raw to contain the vocal message
    int fd=open("audio.raw",O_TRUNC | O_CREAT | O_RDWR,0666);
    if(fd==-1) error("Open file audio");

    memset(buffer,0,sizeof(buffer));

    printf("Press any key to stop recording\n");

    //Recording until a key is not pressed
    while (!kbhit()) {
      
      //Through 'rec' is possibile to fill 'buffer' with symbols of vocal audio
      rc = snd_pcm_readi(rec, buffer, frames);
      if (rc == -EPIPE) {
        snd_pcm_prepare(rec);
      } else 
      if (rc < 0) error("Error from read");

      //Fill the file with the entire vocal message
      rc = write(fd, buffer, size);
      if (rc ==-1) error("Write error");
    }
    (void)getch();

    return fd;
}

//Error handler
void error(char* msg){
	printf("Error in %s\n",msg);
	exit(EXIT_FAILURE);
}


//Thread for sending messages
void* handler(void* args){
		
	while(1){
		RecvMessagesUDP(fd_conn_udp_h,&server_struct_udp_h);
	}
	pthread_exit(NULL);
}

//Handler of sigint
void sig_handler(int sig){

	pthread_cancel(thread);
	pthread_join(thread,NULL);

	//Tell that the client is leaving the chat
	if(check_server!=0){

		int fromlen=sizeof(server_struct_udp);

		strcpy(struct_msg->data,QUIT);
		struct_msg->flag=TEXT;

		//Send msg to the server
		int ret = sendto(fd_conn_udp,struct_msg,sizeof(msg),0,(struct sockaddr *)&server_struct_udp,fromlen);
		if (ret == -1) error(strerror(errno));
	}


	//Free all resources and close fd
	free(struct_msg);
	free(struct_msg_recv);
	free(buffer);

	int ret=close(fd_conn_udp);
	if(ret==-1) error("Closing a socket");
	ret=close(fd_conn_udp_h);
	if(ret==-1) error("Closing a socket");

	snd_pcm_drain(play);
    snd_pcm_close(play);

	snd_config_update_free_global();

	exit(EXIT_SUCCESS);
}

void setAlsaParam(int flag,snd_pcm_stream_t stream){

    snd_pcm_hw_params_t *params;
	unsigned int val;
	int dir=0;


	//Flag 1 to initialize capture PCM
	//else to initialize playback PCM   
	if(flag==1){

		//Open PCM device
		int rc = snd_pcm_open(&rec,"default",stream, 0);
		if (rc < 0) error("Unable to open pcm device");

		//Allocate a hardware parameters object
		snd_pcm_hw_params_alloca(&params);

		//Fill it in with default values.
		snd_pcm_hw_params_any(rec, params);

		//Set the desired hardware parameters

		//Interleaved mode
		snd_pcm_hw_params_set_access(rec, params,SND_PCM_ACCESS_RW_INTERLEAVED);

		//Signed 8-bit format
		snd_pcm_hw_params_set_format(rec, params,SND_PCM_FORMAT_U8);

		// One channel
		snd_pcm_hw_params_set_channels(rec, params, 1);

		//20000 bits/second sampling rate
		val = 20000;
		snd_pcm_hw_params_set_rate_near(rec, params,&val, &dir);

		//Set period size to 8 frames.
		frames = 8;
		snd_pcm_hw_params_set_period_size_near(rec,params, &frames, &dir);

		// Use a buffer large enough to hold one period
		snd_pcm_hw_params_get_period_size(params,&frames, &dir);

	    size = frames; //1 bytes/sample, 1 channels 
		//buffer = (char *) malloc(size);

		//We want to loop for 5 seconds 
		snd_pcm_hw_params_get_period_time(params,&val, &dir);

		// Write the parameters to the driver
		rc = snd_pcm_hw_params(rec, params);
		if (rc < 0) error("Unable to set hw parameters");

	}else{

		//Open PCM device
		int rc = snd_pcm_open(&play,"default",stream, 0);
		if (rc < 0) error("Unable to open pcm device");

		//Allocate a hardware parameters object
		snd_pcm_hw_params_alloca(&params);

		//Fill it in with default values.
		snd_pcm_hw_params_any(play, params);

		//Set the desired hardware parameters

		//Interleaved mode
		snd_pcm_hw_params_set_access(play, params,SND_PCM_ACCESS_RW_INTERLEAVED);

		//Signed 8-bit format
		snd_pcm_hw_params_set_format(play, params,SND_PCM_FORMAT_U8);

		// One channel
		snd_pcm_hw_params_set_channels(play, params, 1);

		//20000 bits/second sampling rate
		val = 20000;
		snd_pcm_hw_params_set_rate_near(play, params,&val, &dir);

		//Set period size to 8 frames.
		frames = 8;
		snd_pcm_hw_params_set_period_size_near(play,params, &frames, &dir);

		// Write the parameters to the driver
		rc = snd_pcm_hw_params(play, params);
		if (rc < 0) error("Unable to set hw parameters");

		// Use a buffer large enough to hold one period
		snd_pcm_hw_params_get_period_size(params,&frames, &dir);

	    size = frames; //1 bytes/sample, 1 channels 
		buffer = (char *) malloc(size);

		snd_pcm_hw_params_get_period_time(params,&val, &dir);

	}
}

void SendMessagesUDP(int fd_conn,struct sockaddr_in* server_struct){

	//Command to close the program
	char* quit_command=QUIT;
	int quit_command_len=strlen(quit_command);

	int ret;
	int finish=1;

	char* aux;
	char txt[1024];
	memset(txt,0,sizeof(txt));

	int fromlen=sizeof(*server_struct);

	//Read from shell
	if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

	int msg_len=strlen(txt);
	
	txt[msg_len - 1] = '\0';

	//Check if the message the user want to send is vocal or not
	if(strcmp(txt,"vocal")!=0){


		//If 'Quit' is written so send it to server and close the program
		finish=memcmp(txt,quit_command,quit_command_len);

		aux=strchr(txt,'-');
		if(aux!=NULL && strlen(aux)>1  && check_server!=0){

			strcpy(struct_msg->data,txt);

			struct_msg->flag=TEXT;


			//Sendo msg to the server
			ret = sendto(fd_conn,struct_msg,sizeof(msg),0,(struct sockaddr *)server_struct,fromlen);
			if (ret == -1) error(strerror(errno));
		}
		//Chekc if 'Quit' is written
		else if(finish==0){

			//Check if the Server is still open
			if(check_server!=0){

				strcpy(struct_msg->data,txt);

				struct_msg->flag=TEXT;

				//Sendo msg to the server
				ret = sendto(fd_conn,struct_msg,sizeof(*struct_msg),0,(struct sockaddr *)server_struct,fromlen);
				if (ret == -1) error(strerror(errno));

			}
			
			//Free all resources and close fd
			free(struct_msg);
			free(struct_msg_recv);

			pthread_cancel(thread);
			pthread_join(thread,NULL);

			ret=close(fd_conn_udp);
			if(ret==-1) error("Closing a socket");
			ret=close(fd_conn_udp_h);
			if(ret==-1) error("Closing a socket");

			exit(EXIT_SUCCESS);

		}else if(check_server!=0){
			printf("Message form is 'Addressee-Messagge' or 'vocal'\n");
		}else{
			printf("Server is closed,please quit the program\n");
		}


	}else{

		//If the user want to send a vocal message

		printf("\nPress enter to start recording");
    	int k=getchar();

    	//initialize capture PCM 'rec'
    	setAlsaParam(1,SND_PCM_STREAM_CAPTURE);

    	//Recording and then the file descriptor of the audio is returned
		int fd_file=Recording(rec,frames,buffer,size);

		struct stat st;
  		stat("audio.raw", &st);
  		//To obtain the dimension of the audio file
  		int audio_len=st.st_size;

  		//Set the pointer at the beginning of the audio file
  		lseek(fd_file,0,SEEK_SET);

  		printf("Insert the addressee of the vocal: ");

  		memset(txt,0,1024);

  		//Read the name of the addressee and save on 'struct_msg->addressee'
  		if(fgets(txt, sizeof(txt), stdin)!=(char*)txt) error("Reading from shell");

  		int msg_len=strlen(txt);
		txt[msg_len - 1] = '\0';

		if(strcmp(struct_msg->name,txt)==0){
			printf("You cannot send a message to yourself\n");
			return;
		}

		strcpy(struct_msg->addressee,txt);

  		//Used to check the integrity of messages
  		int check;

  		//Segment 0 is used to tell the server that an audio message is about to be sent
  		//its size and its addressee
  		struct_msg->audio_len=audio_len;
  		struct_msg->seq_num=0;
  		struct_msg->flag=VOCAL;

  		//Sendo msg to the server
		ret = sendto(fd_conn,struct_msg,sizeof(msg),0,(struct sockaddr *)server_struct,fromlen);
		if (ret == -1) error(strerror(errno));

		if(strcmp(struct_msg->addressee,"All")!=0){

			//First check from the server
			ret=recvfrom(fd_conn,&check,sizeof(int),0,NULL,NULL);
			if(ret==-1) error(strerror(errno));

			//Server tell to the client if the addresse is online or not
			if(check!=0){
				printf("%s\n",NOT_ONLINE_ERROR);
				return;
			}

		}

		//Used to count the number of segment
		int count=1;

  		int bytes_sent=0;

  		char buf_aux[1024];
  		memset(buf_aux,0,1024);

  		while(bytes_sent<audio_len){

  			//Read bytes from the audio file
		    int read_bytes=read(fd_file,buf_aux,1024);
		    
		    //Fill the field of the segment
		    memcpy(struct_msg->data,buf_aux,read_bytes);
		    strcpy(struct_msg->addressee,txt);
		    struct_msg->seq_num=count;
		    struct_msg->len_buf=read_bytes;

		    H: 
		  
		  	//Send the segment to the server,it will send it to the correct user
		    ret=sendto(fd_conn,struct_msg,sizeof(msg),0,(struct sockaddr *)server_struct,fromlen);
		    if(ret==-1) error(strerror(errno));
		   
		   	//Check if the user has received the segment correctly
		    ret=recvfrom(fd_conn,&check,sizeof(int),0,NULL,NULL);
		    if(ret==-1) error(strerror(errno));
		  
			//If the segment is ok go ahead with other segments		  
		    if(check==0){
		    	bytes_sent+=read_bytes;
		        count++;
		    }
		    else
		    	//Resend the segment
		    	goto H;

		}

		//Close the rec PCM
	    snd_pcm_drain(rec);
		snd_pcm_close(rec);

		//Close the file descriptor of the audio
		ret=close(fd_file);
		if(ret==-1) error("Closing FD");

	}

}

void SendMessagesTCP(int fd_conn,char* msg){

	int msg_len=strlen(msg);
	int bytes_send=0;

	int ret;

	//deal with partially sent messages
	while(bytes_send<msg_len){
		ret = send(fd_conn, msg + bytes_send, msg_len - bytes_send,0);
		if (ret == -1 && errno == EINTR) continue;
    	if (ret == -1) error("Writing the socket");
    	bytes_send += ret;
	}

}

void RecvMessagesUDP(int fd_conn,struct sockaddr_in* server_struct){
	
	int ret;
	char* user;

	int audio_len;
	int bytes_read=0;

	socklen_t len_struct=sizeof(struct sockaddr_in);

	ret=recvfrom(fd_conn,struct_msg_recv,sizeof(msg),0,NULL,NULL);
	if (ret == -1) error("Reading from the socket");
	
	//Check if message is from the server that tells the list of users
	if(strcmp(struct_msg_recv->name,"list")==0){
		printf("USERS ONLINE:[");
		user=strtok(struct_msg_recv->data,"-");
		while(user){
			printf("|%s|",user);
			user=strtok(NULL,"-");
		}
		printf("]\n");
	}else if(strcmp(struct_msg_recv->name,SERVER_CLOSE)==0){

		printf("%s,please quit the program\n",struct_msg_recv->name);
		check_server=0;	

		pthread_exit(NULL);
	}
	//if the server is sending an error message 
	else if(strcmp(struct_msg_recv->name,USER_ERROR)==0){
		printf("%s\n",struct_msg_recv->name);
	}else if(strcmp(struct_msg_recv->name,NOT_ONLINE_ERROR)==0){
		printf("%s\n",struct_msg_recv->name);

	//Or if the server is sending a message from another user
	}else if(struct_msg_recv->flag==TEXT){

		//Print the time
		time( &rawtime );
		timeinfo = localtime( &rawtime );

		printf("\n[%02d:%02d:%02d-%s]:%s\n",timeinfo->tm_hour, timeinfo->tm_min,timeinfo->tm_sec,struct_msg_recv->name,struct_msg_recv->data);

	//Or if the message is a audio message	
	}else{

		int count=0;

		int fromlen=sizeof(*server_struct);

		int bytes_read=0;

		char audio_file[1024]="audio_client_";

		strcat(audio_file,struct_msg->name);

		strcat(audio_file,".raw");

		//Create a file that will contain the various audio segments in order
		int fd_file=open(audio_file,O_CREAT | O_TRUNC | O_RDWR,0666);

		//if the segment is the 0 so print the sender and the size of the audio
		if(struct_msg_recv->seq_num==0)
			printf("AUDIO RECV OF SIZE [%d] FROM [%s]\n",struct_msg_recv->audio_len,struct_msg_recv->name);

		while(bytes_read<struct_msg_recv->audio_len){

			int recv=recvfrom(fd_conn,struct_msg_recv,sizeof(msg),0,NULL,NULL);

			//Check if the order is correct
			if(struct_msg_recv->seq_num!=count+1){
				ret=sendto(fd_conn_udp,&(struct_msg_recv->seq_num),sizeof(int),0,(struct sockaddr *)&server_struct_udp,fromlen);
			}else{
				int ok=0;
				ret=sendto(fd_conn_udp,&ok,sizeof(int),0,(struct sockaddr *)&server_struct_udp,fromlen);
			
				//write the segment in the file new file audio
				ret=write(fd_file,struct_msg_recv->data,struct_msg_recv->len_buf);
				bytes_read+=ret;
				
				count++;

			}	
		}

		printf("Listening...\n");

		int rc=1;

		lseek(fd_file,0,SEEK_SET);

		memset(buffer,0,sizeof(buffer));

		while (rc!=0) {

		    rc = read(fd_file,buffer,size);
		    if (rc == 0) {
		      break;
	        } 
		    rc = snd_pcm_writei(play,buffer,frames);
		    if (rc == -EPIPE) {
		      snd_pcm_prepare(play);
		    } else if (rc < 0) {
		      fprintf(stderr,"error from writei: %s\n",snd_strerror(rc));
		    }

		}

		//Close the new file audio
		ret=close(fd_file);
		if(ret==-1) error("Closing file");


	}
}

void RecvMessagesTCP(int fd_conn,char* msg){
	
	int ret;

	int bytes_recvs=0;

	//Deal with partially recv messages
	do{
		ret=recv(fd_conn,msg+bytes_recvs,1,0);
		if (ret == -1 && errno == EINTR) continue;
        	if (ret == -1) error("Reading from the socket");
        	if (ret == 0) break;
		bytes_recvs++;
	}while(msg[bytes_recvs-1]!='\n');

	msg[bytes_recvs-1]='\0';

}

int setUpConnectionTCP(struct sockaddr_in *server_struct,int sockaddr_len){

    int ret;

    int fd_conn=socket(AF_INET, SOCK_STREAM, 0);
    if(fd_conn==-1) error("Creating the socket");

    ret = connect(fd_conn, (struct sockaddr*) server_struct, sockaddr_len);
    if(ret==-1) error("Connecting to the server"); 

    return fd_conn;
}

void Login(int fd_conn,int fd_conn_udp,struct sockaddr_in *server_struct){

	int ret;

	char password[1024];
	char username[1024];
	char result[1024];
	int login_result;

    memset(password,0,sizeof(password));
  	memset(username,0,sizeof(username));
  	memset(result,0,sizeof(result));
  	

	printf("Insert username: ");

	if(fgets(username, sizeof(username), stdin)!=(char*)username) error("Reading from shell");

	int msg_username=strlen(username);
	
	username[msg_username - 1] = '\n';

	SendMessagesTCP(fd_conn,username);

	printf("Insert password: ");

	if(fgets(password, sizeof(password), stdin)!=(char*)password) error("Reading from shell");

	int msg_password=strlen(password);

	password[msg_password - 1] = '\n';

	SendMessagesTCP(fd_conn,password);

	int fromlen=sizeof(*server_struct);

	RecvMessagesTCP(fd_conn,result);

	printf("%s\n",result);

	//If the Login is OK send a fake msg to allow the server to save struct of the client
	if((strcmp(result,ERROR)!=0 && strcmp(result,ERROR_PASS)!=0)){
		username[msg_username - 1] = '\0';
		strcpy(struct_msg->name,username);
		ret = sendto(fd_conn_udp," ",1024,0,(struct sockaddr *)server_struct,fromlen);
		if (ret == -1) error(strerror(errno));
		return;
	}
	else{
		free(struct_msg);
		free(struct_msg_recv);
		exit(EXIT_FAILURE);
	}


}

int setUpConnectionUDP(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	server_struct->sin_family =AF_INET;                    
    server_struct->sin_addr.s_addr = inet_addr(server_ip);    
    server_struct->sin_port = htons(NUM_DOOR_UDP);

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Creating a new socket");

	return fd;

}

int setUpConnectionUDP_H(struct sockaddr_in *server_struct,int sockaddr_len){

	int res;

	server_struct->sin_family =AF_INET;                    
    server_struct->sin_addr.s_addr = inet_addr(server_ip);    
    server_struct->sin_port = htons(NUM_DOOR_UDP_H);

	//Inizialize a new socket
	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Creating a new socket");

	return fd;

}

int main(int argc, char** argv){

	int ret;
	check_server=0;

	struct_msg=malloc(sizeof(msg));
	if(struct_msg==NULL) error("Allocate memory");
	memset(struct_msg,0,sizeof(msg));
	struct_msg_recv=malloc(sizeof(msg));
	if(struct_msg_recv==NULL) error("Allocate memory");
	memset(struct_msg_recv,0,sizeof(msg));

	setAlsaParam(0,SND_PCM_STREAM_PLAYBACK);

	strcpy(server_ip,ADDR_SERVER);

	if(argc==2){
		strcpy(server_ip,argv[1]);
	}

	struct sigaction act={0};
	act.sa_handler=sig_handler;

	//Install handler of signals
	ret=sigaction(SIGINT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGQUIT,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTERM,&act,NULL);
	if(ret==-1) error("sigaction");
	ret=sigaction(SIGTSTP,&act,NULL);
	if(ret==-1) error("sigaction");

    struct sockaddr_in server_struct_tcp = {0};
    memset(&server_struct_udp,0,sizeof(struct sockaddr_in));

    int sockaddr_len = sizeof(struct sockaddr_in);

    memset(&server_struct_udp,0,sizeof(struct sockaddr_in));

    //Insert the address of the server
    server_struct_tcp.sin_addr.s_addr = inet_addr(server_ip);

    //Only IPv4
    server_struct_tcp.sin_family = AF_INET;

    //To convert host byte order
    server_struct_tcp.sin_port = htons(NUM_DOOR_TCP); 

    int fd_conn_tcp=setUpConnectionTCP(&server_struct_tcp,sockaddr_len);

    fd_conn_udp=setUpConnectionUDP(&server_struct_udp,sockaddr_len);

    fd_conn_udp_h=setUpConnectionUDP_H(&server_struct_udp_h,sockaddr_len);

    Login(fd_conn_tcp,fd_conn_udp_h,&server_struct_udp_h);

    check_server=1;

    ret=close(fd_conn_tcp);
    if(ret==-1) error("Closing TCP connection");

    printf("\n");
	printf("Welcome to this chat,the message form must be 'Addressee-Messagge',using 'All' as Addressee you can send a broadcast,if you want to send a vocal message" 
		" please put 'vocal' as input message,to leave the chat please send 'Quit' \n");
	printf("\n");

    //Create a thread to send messages
    ret=pthread_create(&thread,NULL,handler,NULL);
    if(ret==-1) error("Creating the thread");

    //A while fot receve messages
	while(1){
		SendMessagesUDP(fd_conn_udp,&server_struct_udp);
	}

    

	return 0;


}
