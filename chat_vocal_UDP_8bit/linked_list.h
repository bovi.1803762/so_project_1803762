#pragma once

typedef struct UserListItem{
	struct UserListItem* next;
	char* username;
	struct sockaddr_in* client_addr;
}UserListItem;

typedef struct UserHead{
	int count;
	UserListItem* head;
}UserHead;

void List_init_node(char* username,struct sockaddr_in* client_addr,UserListItem* new_node);
void List_insert(UserHead* list,UserListItem* nodo);
void UserListPrint(UserHead* list);
void List_init(UserHead* head);
int List_find_username(UserHead* list,char* username);
struct sockaddr_in* Find_client_struct(UserHead* user_list,char* dst);
char* Find_client_name(UserHead* user_list,struct sockaddr_in* client_addr);
char* List_delete(UserHead* list,char* name);
//int Find_client_fd(UserHead* user_list,char* username);

